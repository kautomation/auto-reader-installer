# auto-reader-installer
* chmod +x installer.sh
* ./installer.sh {PORT}

# server side
* enable AWS server {PORT}
* nc -lv {PORT}


echo "*/5 * * * * sudo systemctl restart terminal.service" >> restartcron   
echo "*/30 * * * * sudo systemctl restart persistance.service" >> restartcron   
echo "0 */4 * * * sudo reboot" >> restartcron   
crontab restartcron   
rm restartcron